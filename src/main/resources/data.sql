--CREATE TABLE IF NOT EXISTS SV_PRODUTO(
--	CD_PRODUTO SERIAL PRIMARY KEY,
--	NM_PRODUTO VARCHAR(100) UNIQUE NOT NULL,
--	DS_PRODUTO TEXT,
--	QT_PRODUTO REAL,
--	VL_PRECO_VENDA NUMERIC(10, 2) NOT NULL,
--	VL_PRECO_COMPRA NUMERIC(10, 2) NOT NULL,
--	TP_UNIDADE_MEDIDA VARCHAR(20) NOT NULL,
--	TS_INCLUSAO DATE NOT NULL,
--	TS_ALTERACAO DATE
--);
--
--CREATE TABLE IF NOT EXISTS SV_ROLE(
--	CD_ROLE SERIAL PRIMARY KEY,
--	NM_ROLE VARCHAR(30) UNIQUE NOT NULL
--);
--
--CREATE TABLE IF NOT EXISTS SV_SERVICO(
--	CD_SERVICO SERIAL PRIMARY KEY,
--	NM_SERVICO VARCHAR(100) UNIQUE NOT NULL,
--	DS_SERVICO TEXT,
--	VL_SERVICO NUMERIC(10, 2) NOT NULL,
--	TS_INCLUSAO DATE NOT NULL,
--	TS_ALTERACAO DATE
--);
--
--CREATE TABLE IF NOT EXISTS SV_USUARIO(
--	CD_USUARIO SERIAL PRIMARY KEY,
--	NM_USUARIO VARCHAR(100) UNIQUE NOT NULL,
--	NM_SENHA VARCHAR(255) NOT NULL,
--	NM_EMAIL VARCHAR(200) UNIQUE NOT NULL,
--	IC_ATIVA BOOLEAN NOT NULL,
--	TS_INCLUSAO DATE NOT NULL,
--	TS_ALTERACAO DATE
--);
--
--CREATE TABLE IF NOT EXISTS SV_CLIENTE(
--	CD_CLIENTE SERIAL PRIMARY KEY,
--	NM_CLIENTE VARCHAR(100) UNIQUE NOT NULL,
--	NM_SOBRENOME VARCHAR(100),
--	NM_RAZAO_SOCIAL VARCHAR(200) UNIQUE,
--	NM_EMAIL VARCHAR(200) UNIQUE NOT NULL,
--	NU_CPF VARCHAR(14) UNIQUE,
--	NU_CNPJ VARCHAR(18) UNIQUE,
--	NU_TELEFONE VARCHAR(11),
--	NU_CELULAR VARCHAR(11),
--	IS_FORNECEDOR BOOLEAN NOT NULL,
--	TS_INCLUSAO DATE NOT NULL,
--	TS_ALTERACAO DATE
--);
--
--CREATE TABLE IF NOT EXISTS SV_USERS_ROLES(
--	CD_USUARIO INTEGER NOT NULL,
--	CD_ROLE INTEGER NOT NULL,
--	CONSTRAINT FK_CD_USUARIO FOREIGN KEY (CD_USUARIO)
--		REFERENCES SV_USUARIO(CD_USUARIO)
--		ON DELETE CASCADE,
--	CONSTRAINT FK_CD_ROLE FOREIGN KEY (CD_ROLE)
--		REFERENCES SV_ROLE(CD_ROLE)
--		ON DELETE CASCADE
--);
--
--CREATE TABLE IF NOT EXISTS SV_ORDEM_SERVICO(
--	CD_ORDEM_SERVICO SERIAL PRIMARY KEY,
--	CD_CLIENTE INTEGER NOT NULL,
--	CD_RESPONSAVEL INTEGER NOT NULL,
--	CD_STATUS VARCHAR(20) NOT NULL,
--	DT_INICIO DATE NOT NULL,
--	DT_FINAL DATE,
--	QT_GARANTIA INT NOT NULL,
--	DS_ORDEM_SERVICO TEXT,
--	DS_DEFEITO TEXT,
--	DS_OBSERVACAO TEXT,
--	DS_LAUDO TEXT,
--	TS_INCLUSAO DATE NOT NULL,
--	TS_ALTERACAO DATE,
--	CONSTRAINT FK_CD_CLIENTE FOREIGN KEY (CD_CLIENTE)
--		REFERENCES SV_CLIENTE(CD_CLIENTE),
--	CONSTRAINT FK_CD_RESPONSAVEL FOREIGN KEY (CD_RESPONSAVEL)
--		REFERENCES SV_USUARIO(CD_USUARIO)
--);
--
--CREATE TABLE IF NOT EXISTS SV_EMPRESA(
--	CD_EMPRESA SERIAL PRIMARY KEY,
--	NM_NOME VARCHAR(100) UNIQUE NOT NULL
--);

insert into sv_usuario(cd_usuario,  NM_EMAIL, ic_ativa, nm_usuario, nm_senha, ts_inclusao) 
values(1,'fulano@gmail.com', '1', 'fulano', '$2a$10$2PAVn9gtH6J.2CT23ud3uec2yPooAjXHPEjQ3MRlQAiikuzRKffBW', CURRENT_DATE);
insert into sv_usuario(cd_usuario,  NM_EMAIL, ic_ativa, nm_usuario, nm_senha, ts_inclusao) 
values(2,'claudio@gmail.com', '1', 'claudio', '$2a$10$2PAVn9gtH6J.2CT23ud3uec2yPooAjXHPEjQ3MRlQAiikuzRKffBW', CURRENT_DATE);
--senha(123456789)

insert into sv_role values(1, 'ROLE_ADMIN');
insert into sv_role values(2,'ROLE_USER');

insert into sv_users_roles values(1, 1);
insert into sv_users_roles values(2, 2);