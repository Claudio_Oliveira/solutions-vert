package com.solutions.vert.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import com.solutions.vert.dto.servico.ServicoDto;

public interface ServicoService {

	/**
	 * 
	 * @param of
	 * @return
	 */
	Page<ServicoDto> findPaginated(PageRequest of);

	/**
	 * 
	 * @param id
	 * @return
	 */
	ServicoDto buscarServicoPorId(Long id);

	/**
	 * 
	 * @param servicoDto
	 */
	void criarServico(ServicoDto servicoDto);

	/**
	 * 
	 * @param id
	 */
	void deletarServicoId(Long id);

}
