package com.solutions.vert.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.solutions.vert.dto.produto.ProdutoDto;

public interface ProdutoService {

	ProdutoDto buscarProdutoPorId(Long id);

	List<ProdutoDto> buscarTodosProdutos();

	void deletarProdutoId(Long id);

	void criarProduto(ProdutoDto produtoDto);

	Page<ProdutoDto> findPaginated(Pageable pageable);

}
