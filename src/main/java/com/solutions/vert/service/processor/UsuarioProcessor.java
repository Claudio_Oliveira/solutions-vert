package com.solutions.vert.service.processor;

import java.util.Optional;

import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.solutions.vert.model.Usuario;
import com.solutions.vert.repository.UsuarioRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class UsuarioProcessor {

	@Autowired
	UsuarioRepository usuarioRepository;
	
	public Usuario exixtsName(String name) {
		log.info("Start - UsuarioProcessor.existsName - NOME: {}", name);
		Optional<Usuario> optional = this.usuarioRepository.findByNome(name);
		if (!optional.isPresent()) {
			log.error("ObjectNotFoundException");
			throw new ObjectNotFoundException(name, "Usuário não encontrado");
		}

		Usuario usuario = optional.get();

		log.info("End - ClienteProcessor.existsName - USUÁRIO: {}", usuario);
		return usuario;
	}
}
