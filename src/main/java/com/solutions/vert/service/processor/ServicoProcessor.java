package com.solutions.vert.service.processor;

import java.util.Optional;

import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.solutions.vert.model.Servico;
import com.solutions.vert.repository.ServicoRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ServicoProcessor {

	@Autowired
	private ServicoRepository servicoRepository;

	public Servico exists(Long id) {
		log.info("Start - ServicoProcessor.exists - ID: {}", id);
		Optional<Servico> optional = this.servicoRepository.findById(id);
		if (!optional.isPresent()) {
			log.error("ObjectNotFoundException");
			throw new ObjectNotFoundException(id, "Serviço não encontrado");
		}

		log.info("Start - ServicoProcessor.exists - Serviço: {}", optional.get());
		return optional.get();
	}

}
