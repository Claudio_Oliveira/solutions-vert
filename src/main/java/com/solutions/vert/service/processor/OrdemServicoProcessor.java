package com.solutions.vert.service.processor;

import java.util.Optional;

import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.solutions.vert.model.OrdemServico;
import com.solutions.vert.repository.OrdemServicoRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class OrdemServicoProcessor {

	@Autowired
	OrdemServicoRepository ordemServicoRepository;
	
	public OrdemServico exists(Long id) {
		log.info("Start - OrdemServicoProcessor.exists - ID: {}", id);
		Optional<OrdemServico> optional = this.ordemServicoRepository.findById(id);
		if (!optional.isPresent()) {
			log.error("ObjectNotFoundException");
			throw new ObjectNotFoundException(id, "Ordem de Serviço não encontrado");
		}
		
		
		log.info("End - OrdemServicoProcessor.exists - Ordem Serviço: {}", optional.get());
		return optional.get();
	}
	
}
