package com.solutions.vert.service.processor;

import java.util.Optional;

import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.solutions.vert.model.cliente.Cliente;
import com.solutions.vert.repository.ClienteRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ClienteProcessor {

	@Autowired
	private ClienteRepository clienteRepository;

	public Cliente exixtsName(String name) {
		log.info("Start - ClienteProcessor.existsName - NOME: {}", name);
		Optional<Cliente> optional = this.clienteRepository.findByNome(name);
		if (!optional.isPresent()) {
			log.error("ObjectNotFoundException");
			throw new ObjectNotFoundException(name, "Cliente não encontrado");
		}

		Cliente cliente = optional.get();

		log.info("End - ClienteProcessor.existsName - CLIENTE: {}", cliente);
		return cliente;
	}

}
