package com.solutions.vert.service;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import com.solutions.vert.dto.ordem.OrdemServicoDto;
import com.solutions.vert.dto.ordem.OrdemServicoDtoFull;

public interface OrdemServicoService {

	/**
	 * 
	 * @param id
	 * @return OrdemServicoDto
	 */
	OrdemServicoDto buscarOrdemServico(Long id);

	/**
	 * 
	 * @param pageable
	 * @return Page<OrdemServicoDtoFull>
	 */
	Page<OrdemServicoDtoFull> findPaginated(PageRequest pageable);

	/**
	 * 
	 * @param ordemServicoDto
	 */
	void criarOrdemServico(@Valid OrdemServicoDto ordemServicoDto);

}
