package com.solutions.vert.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import com.solutions.vert.dto.cliente.ClienteDto;

public interface ClienteService {

	/**
	 * 
	 * @param clienteDto
	 */
	void criarCliente(ClienteDto clienteDto);

	/**
	 * 
	 * @return
	 */
	List<ClienteDto> buscarTodosClientes();

	/**
	 * 
	 * @param id
	 */
	void delete(Long id);

	/**
	 * 
	 * @param id
	 * @return
	 */
	ClienteDto buscarClienteId(Long id);

	/**
	 * 
	 * @param of
	 * @return
	 */
	Page<ClienteDto> findPaginated(PageRequest of);

}
