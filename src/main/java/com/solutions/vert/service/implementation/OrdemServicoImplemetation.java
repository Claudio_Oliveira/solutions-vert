package com.solutions.vert.service.implementation;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.solutions.vert.dto.ordem.OrdemServicoDto;
import com.solutions.vert.dto.ordem.OrdemServicoDtoFull;
import com.solutions.vert.model.OrdemServico;
import com.solutions.vert.model.Usuario;
import com.solutions.vert.model.cliente.Cliente;
import com.solutions.vert.repository.OrdemServicoRepository;
import com.solutions.vert.service.OrdemServicoService;
import com.solutions.vert.service.processor.ClienteProcessor;
import com.solutions.vert.service.processor.OrdemServicoProcessor;
import com.solutions.vert.service.processor.UsuarioProcessor;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class OrdemServicoImplemetation implements OrdemServicoService {

	@Autowired
	OrdemServicoProcessor ordemServicoProcessor;

	@Autowired
	ClienteProcessor clienteProcessor;

	@Autowired
	UsuarioProcessor usuarioProcessor;

	@Autowired
	OrdemServicoRepository ordemServicoRepository;

	@Autowired
	ModelMapper mapper;

	@Override
	public OrdemServicoDto buscarOrdemServico(Long id) {
		log.info("Start - OrdemServicoImplemetation.buscarOrdemServico - ID: {}", id);

		OrdemServico ordemServico = this.ordemServicoProcessor.exists(id);

		OrdemServicoDto ordemServicoDto = this.mapper.map(ordemServico, OrdemServicoDto.class);

		log.info("End - OrdemServicoImplemetation.buscarOrdemServico - ID: {}", id);
		return ordemServicoDto;
	}

	@Override
	public Page<OrdemServicoDtoFull> findPaginated(PageRequest pageable) {
		log.info("Start - OrdemServicoImplemetation.findPaginated");
		int pageSize = pageable.getPageSize();
		int currentPage = pageable.getPageNumber();
		int startItem = currentPage * pageSize;
		List<OrdemServicoDtoFull> list;
		List<OrdemServicoDtoFull> ordemServicoDtos = this.buscarTodosOrdemServico();

		if (ordemServicoDtos.size() <= startItem) {
			list = Collections.emptyList();
		} else {
			int toIndex = Math.min(startItem + pageSize, ordemServicoDtos.size());
			list = ordemServicoDtos.subList(startItem, toIndex);
		}

		Page<OrdemServicoDtoFull> OrdemServicoPage =
				new PageImpl<OrdemServicoDtoFull>(list, PageRequest.of(currentPage, pageSize), ordemServicoDtos.size());

		log.info("End - OrdemServicoImplemetation.findPaginated");
		return OrdemServicoPage;
	}

	private List<OrdemServicoDtoFull> buscarTodosOrdemServico() {
		log.info("Start - OrdemServicoImplemetation.buscarTodosOrdemServico");
		List<OrdemServico> ordemServicos = this.ordemServicoRepository.findAll();

		List<OrdemServicoDtoFull> ordemServicoDtos =
				ordemServicos
						.parallelStream()
						.map(or -> this.mapper.map(or, OrdemServicoDtoFull.class))
						.collect(Collectors.toList());

		ordemServicoDtos.sort(Comparator.comparing(OrdemServicoDtoFull::getId));

		log.info("End - OrdemServicoImplemetation.buscarTodosOrdemServico");
		return ordemServicoDtos;
	}

	@Override
	public void criarOrdemServico(@Valid OrdemServicoDto ordemServicoDto) {
		log.info("Start - OrdemServicoImplemetation.criarOrdemServico - Ordem: {}", ordemServicoDto);
		OrdemServico ordemServico = this.mapper.map(ordemServicoDto, OrdemServico.class);

		Usuario usuario = this.usuarioProcessor.exixtsName(ordemServicoDto.getResponsavel());
		Cliente cliente = this.clienteProcessor.exixtsName(ordemServicoDto.getCliente());

		ordemServico.setCliente(cliente);
		ordemServico.setResponsavel(usuario);

		this.ordemServicoRepository.save(ordemServico);

		log.info("End - OrdemServicoImplemetation.criarOrdemServico");
	}

}
