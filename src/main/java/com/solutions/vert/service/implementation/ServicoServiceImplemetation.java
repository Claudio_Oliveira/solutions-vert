package com.solutions.vert.service.implementation;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.solutions.vert.dto.servico.ServicoDto;
import com.solutions.vert.model.Servico;
import com.solutions.vert.repository.ServicoRepository;
import com.solutions.vert.service.ServicoService;
import com.solutions.vert.service.processor.ServicoProcessor;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ServicoServiceImplemetation implements ServicoService {

	@Autowired
	ServicoRepository servicoRepository;

	@Autowired
	ServicoProcessor servicoProcessor;
	
	@Autowired
	ModelMapper mapper;

	public List<ServicoDto> buscarTodosServicos() {
		log.info("Start - ServicoServiceImplemetation.buscarTodosServicos");
		List<Servico> servicos = this.servicoRepository.findAll();

		List<ServicoDto> servicoDtos =
				servicos.stream().map(ser -> this.mapper.map(ser, ServicoDto.class)).collect(Collectors.toList());
		servicoDtos.sort(Comparator.comparing(ServicoDto::getId));

		log.info("End - ServicoServiceImplemetation.buscarTodosServicos");
		return servicoDtos;
	}

	@Override
	public Page<ServicoDto> findPaginated(PageRequest pageable) {
		log.info("Start - ServicoServiceImplemetation.findPaginated");
		int pageSize = pageable.getPageSize();
		int currentPage = pageable.getPageNumber();
		int startItem = currentPage * pageSize;
		List<ServicoDto> list;
		List<ServicoDto> servicoDtos = this.buscarTodosServicos();

		if (servicoDtos.size() <= startItem) {
			list = Collections.emptyList();
		} else {
			int toIndex = Math.min(startItem + pageSize, servicoDtos.size());
			list = servicoDtos.subList(startItem, toIndex);
		}

		Page<ServicoDto> servicoPage =
				new PageImpl<ServicoDto>(list, PageRequest.of(currentPage, pageSize), servicoDtos.size());

		log.info("End - ServicoServiceImplemetation.findPaginated");
		return servicoPage;
	}

	@Override
	public ServicoDto buscarServicoPorId(Long id) {
		log.info("Start - ServicoServiceImplemetation.buscarServicoPorId - ID: {}", id);

		Servico servico = this.servicoProcessor.exists(id);
		ServicoDto servicoDto = this.mapper.map(servico, ServicoDto.class);

		log.info("Start - ServicoServiceImplemetation.buscarServicoPorId - ServicoDto: {}", id);
		return servicoDto;
	}

	@Override
	public void criarServico(ServicoDto servicoDto) {
		log.info("Start - ServicoServiceImplemetation.criarServico");
		Servico servico = this.mapper.map(servicoDto, Servico.class);

		this.servicoRepository.save(servico);
		log.info("End - ServicoServiceImplemetation.criarServico");
	}

	@Override
	public void deletarServicoId(Long id) {
		log.info("Start - ServicoServiceImplemetation.deletarServicoId - ID: {}", id);

		Servico servico = this.servicoProcessor.exists(id);

		this.servicoRepository.delete(servico);

		log.info("End - ServicoServiceImplemetation.deletarServicoId");
	}

}
