package com.solutions.vert.service.implementation;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.solutions.vert.dto.produto.ProdutoDto;
import com.solutions.vert.model.Produto;
import com.solutions.vert.repository.ProdutoRepository;
import com.solutions.vert.service.ProdutoService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProdutoServiceImplementation implements ProdutoService {

	@Autowired
	private ProdutoRepository produtoRepository;

	@Autowired
	private ModelMapper mapper;

	@Override
	public void criarProduto(ProdutoDto produtoDto) {
		log.info("Start - ProdutoServiceImplementation.criarProduto - Produto: {}", produtoDto);

		if (produtoDto.getId() != null)
			produtoDto.setDataAlterasao(LocalDate.now());

		Produto produto = this.mapper.map(produtoDto, Produto.class);
		produto = this.produtoRepository.save(produto);

		log.info("End - ProdutoServiceImplementation.criarProduto - Produto: {}", produto);
	}

	@Override
	public List<ProdutoDto> buscarTodosProdutos() {
		log.info("Start - ProdutoServiceImplementation.buscarTodosProdutos");

		List<Produto> produtos = this.produtoRepository.findAll();

		List<ProdutoDto> produtoDtos =
				produtos.stream().map(p -> this.mapper.map(p, ProdutoDto.class)).collect(Collectors.toList());
		produtoDtos.sort(Comparator.comparing(ProdutoDto::getId));
		
		log.info("End - ProdutoServiceImplementation.buscarTodosProdutos");
		return produtoDtos;
	}

	@Override
	public ProdutoDto buscarProdutoPorId(Long id) {
		log.info("Start - ProdutoServiceImplementation.buscarProdutoId");
		Optional<Produto> optional = this.produtoRepository.findById(id);

		ProdutoDto produtoDto = this.mapper.map(optional.get(), ProdutoDto.class);

		log.info("End - ProdutoServiceImplementation.buscarProdutoId");
		return produtoDto;
	}

	@Override
	public void deletarProdutoId(Long id) {
		log.info("Start - ProdutoServiceImplementation.deletarProdutoId");

		this.produtoRepository.deleteById(id);

		log.info("End - ProdutoServiceImplementation.deletarProdutoId");

	}

	@Override
	public Page<ProdutoDto> findPaginated(Pageable pageable) {
		int pageSize = pageable.getPageSize();
		int currentPage = pageable.getPageNumber();
		int startItem = currentPage * pageSize;
		List<ProdutoDto> list;
		List<ProdutoDto> produtos = this.buscarTodosProdutos();

		if (produtos.size() <= startItem) {
			list = Collections.emptyList();
		} else {
			int toIndex = Math.min(startItem + pageSize, produtos.size());
			list = produtos.subList(startItem, toIndex);
		}

		Page<ProdutoDto> produtoPage =
				new PageImpl<ProdutoDto>(list, PageRequest.of(currentPage, pageSize), produtos.size());

		return produtoPage;

	}
}
