package com.solutions.vert.service.implementation;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.hibernate.ObjectNotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.solutions.vert.dto.cliente.ClienteDto;
import com.solutions.vert.model.cliente.Cliente;
import com.solutions.vert.model.cliente.Fisica;
import com.solutions.vert.model.cliente.Juridica;
import com.solutions.vert.repository.ClienteRepository;
import com.solutions.vert.service.ClienteService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ClienteServiceImplemetation implements ClienteService {

	@Autowired
	ClienteRepository clienteRepository;

	@Autowired
	ModelMapper mapper;

	@Override
	public void criarCliente(ClienteDto clienteDto) {
		log.info("Start - ClienteServiceImplemetation.criarCliente - {}", clienteDto);
		Cliente cliente;
		if (clienteDto.isTipoCliente())
			cliente = this.mapper.map(clienteDto, Juridica.class);
		else
			cliente = this.mapper.map(clienteDto, Fisica.class);

		this.clienteRepository.save(cliente);

		log.info("End - ClienteServiceImplemetation.criarCliente");

	}

	@Override
	public List<ClienteDto> buscarTodosClientes() {
		log.info("Start - ClienteServiceImplemetation.buscarTodosClientes");

		List<Cliente> clientes = this.clienteRepository.findAll();

		List<ClienteDto> clienteDtos =
				clientes.parallelStream().map(c -> this.mapper.map(c, ClienteDto.class)).collect(Collectors.toList());
		clienteDtos.sort(Comparator.comparing(ClienteDto::getId));

		log.info("End - ClienteServiceImplemetation.buscarTodosClientes");
		return clienteDtos;
	}

	@Override
	public void delete(Long id) {
		log.info("Start - ClienteServiceImplemetation.delete - ID: {}", id);

		Cliente cliente =
				this.clienteRepository
						.findById(id)
						.orElseThrow(() -> new ObjectNotFoundException("O cliente de ID:", null));

		this.clienteRepository.delete(cliente);

		log.info("End - ClienteServiceImplemetation.delete - ID: {}", id);
	}

	@Override
	public ClienteDto buscarClienteId(Long id) {
		log.info("Start - ClienteServiceImplemetation.buscarClienteId - ID: {}", id);
		Optional<Cliente> optional = this.clienteRepository.findById(id);

		ClienteDto clienteDto = this.mapper.map(optional.get(), ClienteDto.class);

		log.info("Start - ClienteServiceImplemetation.buscarClienteId - ID: {}", id);
		return clienteDto;
	}

	@Override
	public Page<ClienteDto> findPaginated(PageRequest pageable) {
		int pageSize = pageable.getPageSize();
		int currentPage = pageable.getPageNumber();
		int startItem = currentPage * pageSize;
		List<ClienteDto> list;
		List<ClienteDto> clienteDtos = this.buscarTodosClientes();

		if (clienteDtos.size() <= startItem) {
			list = Collections.emptyList();
		} else {
			int toIndex = Math.min(startItem + pageSize, clienteDtos.size());
			list = clienteDtos.subList(startItem, toIndex);
		}

		Page<ClienteDto> clientePage =
				new PageImpl<ClienteDto>(list, PageRequest.of(currentPage, pageSize), clienteDtos.size());

		return clientePage;
	}

}
