package com.solutions.vert.service.implementation;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.solutions.vert.dto.usuario.UsuarioDto;
import com.solutions.vert.model.Usuario;
import com.solutions.vert.repository.UsuarioRepository;
import com.solutions.vert.service.UsuarioService;

@Service
public class UsuarioServiceImplemetation implements UsuarioService {

	@Autowired
	UsuarioRepository usuarioRepository;

	@Autowired
	ModelMapper mapper;

	@Override
	public List<UsuarioDto> buscarTodosUsuarios() {

		List<Usuario> usuarios = this.usuarioRepository.findAll();

		List<UsuarioDto> usuarioDtos =
				usuarios.parallelStream().map(us -> this.mapper.map(us, UsuarioDto.class)).collect(Collectors.toList());

		return usuarioDtos;
	}

}
