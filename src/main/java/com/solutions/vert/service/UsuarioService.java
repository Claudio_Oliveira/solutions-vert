package com.solutions.vert.service;

import java.util.List;

import com.solutions.vert.dto.usuario.UsuarioDto;

public interface UsuarioService {

	/**
	 * 
	 * @return List<UsuarioDto>
	 */
	List<UsuarioDto> buscarTodosUsuarios();

}
