package com.solutions.vert.dto.usuario;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UsuarioDto implements Serializable {

	private static final long serialVersionUID = -8815566627476281543L;

	private Long id;

	private String nome;
}
