package com.solutions.vert.dto.cliente;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ClienteDto implements Serializable {

	private static final long serialVersionUID = 4979338140082983612L;

	private Long id;

	@NotBlank
	private String nome;

	private String sobreNome;

	private String razaoSocial;

	private String cpf;

	private String cnpj;

	private String email;

	private String telefone;

	private String celular;

	private boolean tipoCliente;

	public String isTelefone() {
		return this.telefone.isEmpty() ? null : this.telefone;
	}

}
