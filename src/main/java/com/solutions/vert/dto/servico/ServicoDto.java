package com.solutions.vert.dto.servico;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Lob;
import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ServicoDto implements Serializable {

	private static final long serialVersionUID = 8883500083781647107L;

	private Long id;

	@NotBlank
	private String nome;

	@Lob
	@NotBlank
	private String descricao;

	private BigDecimal preco;

}
