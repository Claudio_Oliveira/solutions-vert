package com.solutions.vert.dto.ordem;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Lob;
import javax.validation.constraints.NotBlank;

import org.springframework.format.annotation.DateTimeFormat;

import com.solutions.vert.model.StatusOrdem;
import com.solutions.vert.model.Usuario;
import com.solutions.vert.model.cliente.Cliente;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrdemServicoDtoFull implements Serializable {

	private static final long serialVersionUID = 4534558307365186559L;

	private Long id;

	@NotBlank
	private Cliente cliente;

	private StatusOrdem status;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate dataInicio;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate dataFinal;

	@NotBlank
	private Usuario responsavel;

	private int diasGarantia;

	@Lob
	private String descricao;

	@Lob
	private String defeito;

	@Lob
	private String observacao;

	@Lob
	private String laudo;
}
