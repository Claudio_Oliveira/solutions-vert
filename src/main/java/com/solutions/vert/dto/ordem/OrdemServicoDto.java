package com.solutions.vert.dto.ordem;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

import javax.persistence.Lob;
import javax.validation.constraints.NotBlank;

import org.springframework.format.annotation.DateTimeFormat;

import com.solutions.vert.model.StatusOrdem;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrdemServicoDto implements Serializable {

	private static final long serialVersionUID = -7999310108762753665L;

	private Long id;

	@NotBlank
	private String cliente;

	private StatusOrdem status;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date dataInicio;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate dataFinal;

	@NotBlank
	private String responsavel;

	private int diasGarantia;

	@Lob
	private String descricao;

	@Lob
	private String defeito;

	@Lob
	private String observacao;

	@Lob
	private String laudo;

}
