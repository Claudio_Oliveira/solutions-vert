package com.solutions.vert.dto.produto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Lob;
import javax.validation.constraints.NotBlank;

import com.solutions.vert.model.Unidade;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProdutoDto implements Serializable {

	private static final long serialVersionUID = 2140652281082150480L;
	
	private Long id;
	
	@NotBlank
	private String nome;

	@Lob
	@NotBlank
	private String descricao;
	
	private Integer estoque;
	
	private BigDecimal preco;
	
	private BigDecimal precoCompra;
	
	private LocalDate dataAlterasao;
	
	private Unidade unidade;
	

}
