package com.solutions.vert.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/login")
public class LoginController {

	@GetMapping
	public String login() {
		log.info("Start - LoginController-login");

		log.info("End - LoginController-login");
		return "login";
	}

}
