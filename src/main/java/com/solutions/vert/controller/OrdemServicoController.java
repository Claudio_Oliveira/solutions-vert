package com.solutions.vert.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.solutions.vert.dto.ordem.OrdemServicoDto;
import com.solutions.vert.dto.ordem.OrdemServicoDtoFull;
import com.solutions.vert.dto.usuario.UsuarioDto;
import com.solutions.vert.service.OrdemServicoService;
import com.solutions.vert.service.UsuarioService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
@RequestMapping("/ordem-servico")
public class OrdemServicoController {

	@Autowired
	OrdemServicoService ordemServico;
	
	@Autowired
	UsuarioService usuarioService;

	@GetMapping
	public String ordemServico(
		Model model,
		@RequestParam("page") Optional<Integer> page,
		@RequestParam("size") Optional<Integer> size) {
		log.info("Start - OrdemServicoController.ordemServico");

		int currentPage = page.orElse(1);
		int pageSize = size.orElse(5);

		Page<OrdemServicoDtoFull> ordemServicoPage =
				this.ordemServico.findPaginated(PageRequest.of(currentPage - 1, pageSize));

		int totalPages = ordemServicoPage.getTotalPages();

		if (totalPages > 0) {
			List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages).sorted().boxed().toList();
			model.addAttribute("pageNumbers", pageNumbers);
		}

		model.addAttribute("ordemServicoPage", ordemServicoPage);
		model.addAttribute("currentPage", currentPage);
		model.addAttribute("totalPages", totalPages);

		log.info("End - OrdemServicoController.ordemServico");

		return "ordem/ordemServico";

	}

	@GetMapping("/form")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String formulario(OrdemServicoDto ordemServicoDto, Model model) {
		log.info("Start - OrdemServicoController.formulario");

		List<UsuarioDto> usuarios = this.usuarioService.buscarTodosUsuarios();
		
		model.addAttribute("usuarios", usuarios);
		
		log.info("End - OrdemServicoController.formulario");
		return "ordem/cadastro";

	}

	@GetMapping("/form/{id}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String atualizar(Model model, @PathVariable(name = "id") Long id) {
		log.info("Start - OrdemServicoController.formulario");

		OrdemServicoDto ordemServicoDto = this.ordemServico.buscarOrdemServico(id);

		model.addAttribute("ordemServicoDto", ordemServicoDto);

		log.info("End - OrdemServicoController.formulario");
		return "ordem/cadastro";

	}

	@PostMapping("/cadastro")
	@PreAuthorize(value = "hasRole('ROLE_ADMIN')")
	public String criarOrdemServico(@Valid OrdemServicoDto ordemServicoDto, BindingResult result) {
		log.info("Start - OrdemServicoController.criarOrdemServico - {}", ordemServicoDto);
		if (result.hasErrors()) {
			log.error("End - OrdemServicoController.criarOrdemServico - Erro ao criar o ordem de serviço");
			return "ordem/cadastro";
		}

		this.ordemServico.criarOrdemServico(ordemServicoDto);

		log.info("End - OrdemServicoController.criarOrdemServico");

		return "redirect:/ordem-servico";
	}
}
