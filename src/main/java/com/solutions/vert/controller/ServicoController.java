package com.solutions.vert.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.solutions.vert.dto.servico.ServicoDto;
import com.solutions.vert.service.ServicoService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/servico")
public class ServicoController {

	private static final String SERVICO = "servico/servico";
	private static final String CADASTRO_SERVICO = "/servico/cadastro";

	@Autowired
	private ServicoService servicoService;

	@GetMapping
	public String servico(
		Model model,
		@RequestParam("page") Optional<Integer> page,
		@RequestParam("size") Optional<Integer> size) {
		log.info("Start - ServicoController.servico");

		int currentPage = page.orElse(1);
		int pageSize = size.orElse(5);

		Page<ServicoDto> servicoPage = this.servicoService.findPaginated(PageRequest.of(currentPage - 1, pageSize));

		int totalPages = servicoPage.getTotalPages();

		if (totalPages > 0) {
			List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages).sorted().boxed().toList();
			model.addAttribute("pageNumbers", pageNumbers);
		}

		model.addAttribute("servicoPage", servicoPage);
		model.addAttribute("currentPage", currentPage);
		model.addAttribute("totalPages", totalPages);

		log.info("End - ServicoController.servico");
		return SERVICO;

	}

	@GetMapping("/form/{id}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String formulario(Model model, @PathVariable(name = "id") Long id) {
		log.info("Start - ServicoController.formulario - ID: {}", id);

		ServicoDto servicoDto = this.servicoService.buscarServicoPorId(id);
		model.addAttribute("servicoDto", servicoDto);

		log.info("End - ServicoController.formulario - Serviço: {}", servicoDto);
		return CADASTRO_SERVICO;
	}

	@GetMapping("/form")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String formulario(ServicoDto servicoDto) {
		log.info("Start - ServicoController.formulario - Serviço: {}", servicoDto);
		log.info("End - ServicoController.formulario");
		return CADASTRO_SERVICO;
	}

	@PostMapping("/cadastro")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String criarServico(@Valid ServicoDto servicoDto, BindingResult result) {
		log.info("Start - ServicoController.criarServico - Serviço: {}", servicoDto);
		if (result.hasErrors()) {
			return CADASTRO_SERVICO;
		}
		this.servicoService.criarServico(servicoDto);

		log.info("End - ServicoController.criarServico");
		return "redirect:/servico";
	}

	@GetMapping("/delete/{id}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String deleteServicoId(@PathVariable(name = "id") Long id) {
		log.info("Start - ServicoController.deleteServicoId");
		try {
			this.servicoService.deletarServicoId(id);
		}catch (Exception e) {
			log.error(e.getMessage());
			return "redirect:/servico";
		}
		log.info("End - ServicoController.deleteServicoId");

		return "redirect:/servico";
	}

}
