package com.solutions.vert.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.solutions.vert.dto.cliente.ClienteDto;
import com.solutions.vert.service.ClienteService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/cliente")
public class ClienteController {

	@Autowired
	ClienteService clienteService;

	@GetMapping
	public String cliente(
		Model model,
		@RequestParam("page") Optional<Integer> page,
		@RequestParam("size") Optional<Integer> size) {
		log.info("Start - ClienteController.cliente");
		int currentPage = page.orElse(1);
		int pageSize = size.orElse(5);

		Page<ClienteDto> clientePage = this.clienteService.findPaginated(PageRequest.of(currentPage - 1, pageSize));

		int totalPages = clientePage.getTotalPages();

		if (totalPages > 0) {
			List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages).sorted().boxed().toList();
			model.addAttribute("pageNumbers", pageNumbers);
		}

		model.addAttribute("clientePage", clientePage);
		model.addAttribute("currentPage", currentPage);
		model.addAttribute("totalPages", totalPages);
		log.info("End - ClienteController.cliente");
		return "cliente/clientes";
	}

	@GetMapping("/form")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String formulario(ClienteDto clienteDto) {
		log.info("Start - ClienteController.formulario");

		log.info("End - ClienteController.formulario");
		return "cliente/cadastro";

	}

	@GetMapping("/form/{id}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String atualizar(Model model, @PathVariable(name = "id") Long id) {
		log.info("Start - ClienteController.formulario");

		ClienteDto clienteDto = this.clienteService.buscarClienteId(id);

		model.addAttribute("clienteDto", clienteDto);

		log.info("End - ClienteController.formulario");
		return "cliente/cadastro";

	}

	@PostMapping("/cadastro")
	@PreAuthorize(value = "hasRole('ROLE_ADMIN')")
	public String criarCliente(@Valid ClienteDto clienteDto, BindingResult result) {
		log.info("Start - ClienteController.criarCliente - {}", clienteDto);
		if (result.hasErrors()) {
			log.error("End - ClienteController.criarCliente - Erro ao criar o cliente");
			return "cliente/cadastro";
		}

		this.clienteService.criarCliente(clienteDto);

		log.info("End - ClienteController.criarCliente");

		return "redirect:/cliente";
	}

	@GetMapping("/delete/{id}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String deleteCliente(@PathVariable(name = "id") Long id) {
		log.info("Start - ClienteController.deleteCliente - ID: {}", id);

		this.clienteService.delete(id);

		log.info("End - ClienteController.deleteCliente - ID: {}", id);

		return "redirect:/cliente";
	}

}
