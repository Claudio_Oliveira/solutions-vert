package com.solutions.vert.controller;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.solutions.vert.dto.produto.ProdutoDto;
import com.solutions.vert.service.ProdutoService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/produto")
public class ProdutoController {
	
	private static final String LISTAR_PRODUTO = "produto/produto";
	private static final String FORMULARIO_CADASTRO = "produto/cadastro";

	@Autowired
	private ProdutoService produtoService;

	@GetMapping
	public String produto(
		Model model,
		@RequestParam("page") Optional<Integer> page,
		@RequestParam("size") Optional<Integer> size) {
		log.info("Start - ProdutoController.produto");
		
		int currentPage = page.orElse(1);
		int pageSize = size.orElse(5);

		Page<ProdutoDto> produtoPage = this.produtoService.findPaginated(PageRequest.of(currentPage - 1, pageSize));

		int totalPages = produtoPage.getTotalPages();

		if (totalPages > 0) {
			List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages).sorted().boxed().toList();
			model.addAttribute("pageNumbers", pageNumbers);
		}

		model.addAttribute("produtoPage", produtoPage);
		model.addAttribute("currentPage", currentPage);
		model.addAttribute("totalPages", totalPages);
		log.info("End - ProdutoController.produto");
		return LISTAR_PRODUTO;
	}

	@GetMapping("/form/{id}")
	public String formulario(Model model, @PathVariable(name = "id") Long id) {
		log.info("Start - ProdutoController.formulario");

		ProdutoDto produtoDto = this.produtoService.buscarProdutoPorId(id);
		produtoDto.setDataAlterasao(LocalDate.now());

		model.addAttribute("produtoDto", produtoDto);

		log.info("End - ProdutoController.formulario");
		return FORMULARIO_CADASTRO;

	}

	@GetMapping("/delete/{id}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String deleteProdutoId(@PathVariable(name = "id") Long id) {
		log.info("Start - ProdutoController.deleteProdutoId");

		this.produtoService.deletarProdutoId(id);

		log.info("End - ProdutoController.deleteProdutoId");

		return "redirect:/produto";
	}

	@GetMapping("/form")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String formulario(ProdutoDto produtoDto) {
		log.info("Start - ProdutoController.formulario");

		log.info("End - ProdutoController.formulario");
		return FORMULARIO_CADASTRO;

	}

	@PostMapping("/cadastro")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String criarProduto(@Valid ProdutoDto produtoDto, BindingResult result) {
		log.info("Start - ProdutoController.criarProduto - Produto: {}", produtoDto);
		if (result.hasErrors()) {
			log.info("End - ProdutoController.criarProduto");
			return FORMULARIO_CADASTRO;
		}

		this.produtoService.criarProduto(produtoDto);

		log.info("End - ProdutoController.criarProduto");
		return "redirect:/produto";
	}

}
