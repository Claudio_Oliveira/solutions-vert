package com.solutions.vert.controller;

import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.solutions.vert.dto.cliente.ClienteDto;
import com.solutions.vert.dto.produto.ProdutoDto;
import com.solutions.vert.service.ClienteService;
import com.solutions.vert.service.ProdutoService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/home")
public class HomeController {

	@Autowired
	private ClienteService clienteService;

	@Autowired
	private ProdutoService produtoService;

	@GetMapping
	public String home(Model model) {
		
		log.info("Start - HomeController.home");

		List<ProdutoDto> produtosDtos = this.produtoService.buscarTodosProdutos();
		model.addAttribute("produtos", produtosDtos.stream().limit(4).toList());
		
		List<ClienteDto> clienteDtos = this.clienteService.buscarTodosClientes();
		clienteDtos.sort(Comparator.comparing(ClienteDto:: getId));
		model.addAttribute("clientes", clienteDtos.stream().limit(4).toList());

		log.info("End - HomeController.home");
		return "home/home";
	}

}
