package com.solutions.vert.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.solutions.vert.model.Usuario;
import com.solutions.vert.repository.UsuarioRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UsuarioRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Usuario user = this.userRepository.getUserByEmail(email);

		if (user == null) {
			throw new UsernameNotFoundException("Usuario nao encontrado");
		}

		return new VertUserDetails(user);
	}
}
