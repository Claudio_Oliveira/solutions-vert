package com.solutions.vert.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.solutions.vert.model.Servico;

@Repository
public interface ServicoRepository extends JpaRepository<Servico, Long> {

}
