package com.solutions.vert.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.solutions.vert.model.Produto;

@Repository
public interface ProdutoRepository extends JpaRepository<Produto, Long> {

}
