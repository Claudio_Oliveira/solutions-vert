package com.solutions.vert.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.solutions.vert.model.Usuario;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

	@Query(value = "SELECT u.* FROM sv_usuario u WHERE u.nm_email = :email", nativeQuery = true)
	public Usuario getUserByEmail(String email);

	@Query(value = "SELECT u.* FROM sv_usuario u WHERE u.nm_usuario = :name", nativeQuery = true)
	public Optional<Usuario> findByNome(String name);
}
