package com.solutions.vert.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.solutions.vert.model.cliente.Cliente;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long> {

	@Query(value = "SELECT c.* FROM sv_cliente c WHERE c.nm_cliente = :name", nativeQuery = true)
	Optional<Cliente> findByNome(String name);

}
