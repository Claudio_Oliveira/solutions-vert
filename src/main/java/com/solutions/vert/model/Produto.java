package com.solutions.vert.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "sv_produto")
@Table(name = "sv_produto")
public class Produto extends Audit implements Serializable {

	private static final long serialVersionUID = 1966598224760539995L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cd_produto")
	private Long id;

	@NotNull
	@Column(name = "nm_produto", nullable = true)
	private String nome;

	@NotNull
	@Column(name = "ds_produto")
	private String descricao;

	@Column(name = "qt_produto")
	private Integer estoque;

	@Column(name = "vl_preco_venda")
	private BigDecimal preco;

	@Column(name = "vl_preco_compra")
	private BigDecimal precoCompra;

	@Enumerated(EnumType.STRING)
	@Column(name = "tp_unidade_medida")
	private Unidade unidade;

}
