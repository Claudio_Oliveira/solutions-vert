package com.solutions.vert.model.cliente;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.br.CPF;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Fisica extends Cliente {

	private static final long serialVersionUID = -6976238320706526601L;

	@NotNull
	@Column(name = "nm_sobrenome")
	private String sobrenome;

	@CPF
	@NotNull
	@Column(name = "nu_cpf", unique = true)
	@Size(min = 14, max = 14)
	@Default
	private String cpf = "000.000.000-00";

}
