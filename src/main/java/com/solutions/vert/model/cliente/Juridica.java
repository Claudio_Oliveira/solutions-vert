package com.solutions.vert.model.cliente;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.br.CNPJ;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Juridica extends Cliente {

	private static final long serialVersionUID = 7070091554804519537L;

	@Column(name = "nm_razao_social")
	private String razaoSocial;

	@CNPJ
	@Column(name = "nu_cnpj", unique = true)
	@Size(min = 18, max = 18 )
	@Default
	private String cnpj = "00.000.000/0000-00";

}
