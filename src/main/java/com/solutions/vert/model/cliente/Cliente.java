package com.solutions.vert.model.cliente;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.solutions.vert.model.Audit;
import com.solutions.vert.model.OrdemServico;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity(name = "sv_cliente")
@Table(name = "sv_cliente")
public abstract class Cliente extends Audit implements Serializable {

	private static final long serialVersionUID = 7822074365417962706L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cd_cliente")
	private Long id;

	@NotNull
	@Column(name = "nm_cliente", nullable = true, length = 100)
	private String nome;

	@Email
	@Column(name = "nm_email", unique = true)
	private String email;

	@Column(name = "nu_telefone")
	private String telefone;

	@Column(name = "nu_celular")
	private String celular;

	@NotNull
	@Column(name = "is_fornecedor", nullable = true)
	private boolean tipoCliente;

	@OneToMany(targetEntity = OrdemServico.class, mappedBy = "cliente")
	private List<OrdemServico> ordemServicos;
}
