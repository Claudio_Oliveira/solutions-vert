package com.solutions.vert.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity(name = "sv_usuario")
@Table(name = "sv_usuario")
public class Usuario extends Audit implements Serializable {

	private static final long serialVersionUID = -5013101814907986669L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cd_usuario")
	private Long id;

	@NotBlank
	@Column(name = "nm_usuario", nullable = false)
	private String nome;

	@NotBlank
	@Column(name = "nm_senha", nullable = false)
	private String senha;

	@Email
	@NotBlank
	@Column(name = "nm_email", unique = true, nullable = false)
	private String email;

	@Default
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "sv_users_roles", joinColumns = @JoinColumn(name = "cd_usuario"), inverseJoinColumns = @JoinColumn(name = "cd_role"))
	private Set<Role> roles = new HashSet<>();

	@NotBlank
	@Column(name = "ic_ativa", nullable = false)
	private boolean enabled;

	@OneToMany(targetEntity = OrdemServico.class, mappedBy = "responsavel")
	private List<OrdemServico> ordemServicos;

}
