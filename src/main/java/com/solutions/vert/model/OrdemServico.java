package com.solutions.vert.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.solutions.vert.model.cliente.Cliente;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "sv_ordem_servico")
@Table(name = "sv_ordem_servico")
public class OrdemServico extends Audit implements Serializable {

	private static final long serialVersionUID = 6677991939313615645L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cd_ordem_servico")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "cd_cliente", nullable = true)
	private Cliente cliente;

	@Default
	@Enumerated(EnumType.STRING)
	@Column(name = "cd_status", nullable = true)
	private StatusOrdem status = StatusOrdem.ORCAMENTO;

	@Default
	@Column(name = "dt_inicio", nullable = true)
	private LocalDate dataInicio = LocalDate.now();

	@Column(name = "dt_final")
	private LocalDate dataFinal;

	@ManyToOne
	@JoinColumn(name = "cd_responsavel")
	private Usuario responsavel;

	@Column(name = "qt_garantia")
	private int diasGarantia;

	@Lob
	@Column(name = "ds_order_servico")
	private String descricao;

	@Lob
	@Column(name = "ds_defeito")
	private String defeito;

	@Lob
	@Column(name = "ds_observacao")
	private String observacao;

	@Lob
	@Column(name = "ds_laudo")
	private String laudo;

}
