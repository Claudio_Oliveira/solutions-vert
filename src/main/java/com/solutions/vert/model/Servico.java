package com.solutions.vert.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "sv_servico")
@Table(name = "sv_servico")
public class Servico extends Audit implements Serializable {

	private static final long serialVersionUID = -3296397590200125133L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cd_servico")
	private Long id;

	@Column(name = "nm_servico", length = 100, nullable = true)
	private String nome;

	@NotNull
	@Column(name = "ds_servico", length = 200, nullable = true)
	private String descricao;

	@Column(name = "vl_servico")
	private BigDecimal preco;

}
