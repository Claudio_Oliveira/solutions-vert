package com.solutions.vert.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;

@Data
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class Audit {

	@CreatedDate
	@Column(name = "ts_inclusao", updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateInclusao;

	@LastModifiedDate
	@Column(name = "ts_alteracao")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataAlteracao;

}
