package com.solutions.vert.model;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public enum StatusOrdem {

	ABERTO, ORCAMENTO, EM_ANDAMENTO, FINALIZADO, CANCELADO, AGUARDANDO_PECAS, APROVADO;
	
	
	public List<String> getList() {
		return Arrays.asList(ABERTO, ORCAMENTO, EM_ANDAMENTO, FINALIZADO, CANCELADO, AGUARDANDO_PECAS, APROVADO)
				.parallelStream().map(u -> u.toString()).collect(Collectors.toList());
	}

}
