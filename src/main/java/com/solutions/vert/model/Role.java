package com.solutions.vert.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity(name = "sv_role")
@Table(name = "sv_role")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Role {

	@Id
	@Column(name = "cd_role")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "nm_role")
	private String name;

}
