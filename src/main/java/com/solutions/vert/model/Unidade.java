package com.solutions.vert.model;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public enum Unidade {
	UNIDADE, METRO, METRO_QUADRADO, LITRO, QUILOGRAMA, PACOTE, CAIXA, FARDO;
	
	public List<String> getList() {
		return Arrays.asList(UNIDADE, METRO, METRO_QUADRADO, LITRO, QUILOGRAMA, PACOTE, CAIXA, FARDO)
				.parallelStream().map(u -> u.toString()).collect(Collectors.toList());
	}

}
